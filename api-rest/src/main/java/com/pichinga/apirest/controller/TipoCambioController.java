package com.pichinga.apirest.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pichinga.apirest.dto.TipoCambioDTO;
import com.pichinga.apirest.model.TipoCambio;
import com.pichinga.apirest.model.Usuario;
import com.pichinga.apirest.repository.TipoCambioRepository;
import com.pichinga.apirest.service.TipoCambioService;
import com.pichinga.apirest.swagger.TipoCambioApi;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/tipo-cambio")
public class TipoCambioController implements TipoCambioApi {

	@Autowired
	private TipoCambioService tipoCambioService;

	@Autowired
	private TipoCambioRepository tipoCambioRepository;

	@PostMapping
	public Mono<ResponseEntity<TipoCambio>> createTipoCambio(@RequestBody TipoCambio tipoCambio, @RequestHeader("Authorization") String token) {
		return tipoCambioService.createTipoCambio(tipoCambio, token)
				.map(tipoCambioGuardado -> new ResponseEntity<>(tipoCambioGuardado, HttpStatus.ACCEPTED))
				.defaultIfEmpty(new ResponseEntity<>(tipoCambio, HttpStatus.NOT_ACCEPTABLE));
	}

	@PutMapping("/{id}")
	public Mono<ResponseEntity<TipoCambio>> actualizarContacto(@RequestBody TipoCambio tipoCambio,
			@PathVariable Long id,
			@RequestHeader("Authorization") String token) {
		return tipoCambioService.updateTipoCambio(id, tipoCambio,token)
				.map(tipoCambio1 -> new ResponseEntity<>(tipoCambio1, HttpStatus.ACCEPTED))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@GetMapping(value = "/{id}")
	public Mono<ResponseEntity<TipoCambio>> obtenerTipoCambio(@PathVariable Long id, @RequestHeader("Authorization") String token) {
		return tipoCambioService.getTipoCambioById(id, token)
				.map(tipoCambio -> new ResponseEntity<>(tipoCambio, HttpStatus.OK))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@PostMapping("/cambiar")
	public Mono<ResponseEntity<TipoCambioDTO>> cambiar(@RequestBody TipoCambioDTO tipoCambio, @RequestHeader("Authorization") String token) {

		return tipoCambioService.cambiar(tipoCambio,token )
				.map(dato -> new ResponseEntity<>(dato, HttpStatus.OK))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

}
