package com.pichinga.apirest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import com.pichinga.apirest.model.Usuario;
import com.pichinga.apirest.service.UsuarioService;

@RestController
@RequestMapping("api/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService userService;
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public Flux<Usuario>getAllUser(){
		return userService.getAllUser();
	}
	
	@GetMapping("{id}")
	public Mono<ResponseEntity<Usuario>> searchUserById(@PathVariable("id") Long id ) {
		return userService.getUserById(id)
				.map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@PostMapping
	public Mono<ResponseEntity<Usuario>> createUser(@RequestBody Usuario user) {
		return userService.createUser(user)
				.map(userGuardado -> new ResponseEntity<>(userGuardado,HttpStatus.ACCEPTED))
                .defaultIfEmpty(new ResponseEntity<>(user,HttpStatus.NOT_ACCEPTABLE));
	}
	
	@DeleteMapping("{id}")
	public Mono<Void> deleteUserById(@PathVariable("id") Long id ) {
		return userService.deleteUser(id);
	}

}
