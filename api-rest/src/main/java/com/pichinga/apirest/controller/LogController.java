package com.pichinga.apirest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pichinga.apirest.model.Log;
import com.pichinga.apirest.service.LogService;

import reactor.core.publisher.Flux;


@RestController
@RequestMapping("api/log")
public class LogController {
	
	@Autowired
	private LogService logService;
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public Flux<Log> getAllLog(){
		return logService.getAllLog();
	}

}
