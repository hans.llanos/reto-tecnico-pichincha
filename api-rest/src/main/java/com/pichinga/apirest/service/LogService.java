package com.pichinga.apirest.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pichinga.apirest.model.Log;
import com.pichinga.apirest.repository.LogRepository;

import reactor.core.publisher.Flux;

@Component
public class LogService {
	@Autowired
	private LogRepository logRepository;
	
	
	
	public Flux<Log> getAllLog() {
		 return logRepository.findAll();
	}
	

}
