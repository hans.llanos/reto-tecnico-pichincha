package com.pichinga.apirest.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pichinga.apirest.model.Usuario;
import com.pichinga.apirest.repository.UsuarioRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class UsuarioService {
	@Autowired
	private UsuarioRepository userRepository;
	
	public Flux<Usuario> getAllUser() {
		 return userRepository.findAll();
	}
	
	public Flux<Usuario> findOneByEmail(String email){
		return userRepository.findOneByEmail(email);
	}
	
	public Mono<Usuario> getUserById(Long id) {
		 return  userRepository.findById(id);
	}
	
	public Mono<Usuario> createUser(Usuario user) {
		return userRepository.save(user);
	}
	
	public Mono<Usuario> update(Long id, Usuario user) {
	    return userRepository.findById(id).map(Optional::of).defaultIfEmpty(Optional.empty())
	        .flatMap(optionalUser-> {
	          if (optionalUser.isPresent()) {
	        	  user.setId(id);
	        	  return userRepository.save(user);
	          }

	          return Mono.empty();
	        });
	  }
	
	public Mono<Void> deleteUser(Long id) {
		return userRepository.deleteById(id);
	}
	
	

}
