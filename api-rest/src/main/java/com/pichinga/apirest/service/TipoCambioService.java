package com.pichinga.apirest.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pichinga.apirest.dto.TipoCambioDTO;
import com.pichinga.apirest.model.Log;
import com.pichinga.apirest.model.TipoCambio;
import com.pichinga.apirest.repository.LogRepository;
import com.pichinga.apirest.repository.TipoCambioRepository;
import com.pichinga.apirest.security.entity.Users;
import com.pichinga.apirest.security.jwt.JwtProvider;
import com.pichinga.apirest.security.repository.UserRepository;

import io.jsonwebtoken.Claims;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Component
public class TipoCambioService {
	@Autowired
	private TipoCambioRepository tipoCambioRepository;

	@Autowired
	private LogRepository logRepository;

	@Autowired
	private JwtProvider jwt;
	
	@Autowired
	private UserRepository userRepository;
	

	private final static String CREATE = "TIPOCAMBIO->CREATE";
	private final static String UPDATE = "TIPOCAMBIO->UPDATE";
	private final static String FIND = "TIPOCAMBIO->FIND";
	private final static String CONVERTER = "TIPOCAMBIO->CONVERTER";

	public Mono<TipoCambio> createTipoCambio(TipoCambio tipoCambio, String token) {
		guardarLog(CREATE,token, tipoCambio);
		return tipoCambioRepository.save(tipoCambio);
		

	}

	public Mono<TipoCambio> updateTipoCambio(Long id, TipoCambio tipo, String token) {
		
		return tipoCambioRepository.findById(id).map(Optional::of).defaultIfEmpty(Optional.empty())
				.flatMap(optionalTipoCambio -> {
					if (optionalTipoCambio.isPresent()) {
						tipo.setId(id);
						guardarLog(UPDATE,token, tipo);
						return tipoCambioRepository.save(tipo);
					}

					return Mono.empty();
				});
	}

	public Mono<TipoCambio> getTipoCambioById(Long id, String token) {
		guardarLog(FIND,token, id );
		return tipoCambioRepository.findById(id);
	}
	
	public Mono<TipoCambioDTO> cambiar(TipoCambioDTO obj, String token) {
	    Mono<TipoCambio> tipoCambio = tipoCambioRepository.findByMonedaOrigenAndMonedaDestino(obj.getMonedaOrigen(), obj.getMonedaDestino());
	    
	    return tipoCambio.zipWith(tipoCambio)
	    		.map(dato -> {
	    			TipoCambioDTO resultado = new TipoCambioDTO();
					Double montoCambiado = 0.0;
					
	    			montoCambiado = dato.getT1().getTipoCambio() * obj.getMonto();

					resultado.setMonedaOrigen(obj.getMonedaOrigen());
					resultado.setMonedaDestino(obj.getMonedaDestino());
					resultado.setMonto(obj.getMonto());
					resultado.setMontoCambiado(montoCambiado);
					resultado.setTipoCambio(dato.getT1().getTipoCambio());
					guardarLog(CONVERTER,token, obj );
					return resultado;
	            });
	}	
	
	public Mono<Void> guardarLog(String accion, String token, Object dato) {		
		
		String password = token.replace("Bearer ", "");		
		String usuario = getAllClaimsFromToken(password);
		
		return userRepository.findByUsername(usuario).map(Optional::of).defaultIfEmpty(Optional.empty())
				.flatMap(obj -> { 
					if (obj.isPresent()) {
						Log log = new Log();
						
						log.setAccion(accion);
						log.setDetalle(dato.toString());
						log.setUsuario(usuario);
						log.setFechaCreacion(new Date());

						logRepository.save(log);
						return Mono.empty();
					}

					return Mono.empty();
				});

	}
	
	private String getAllClaimsFromToken(String token) {
        Claims claims;
        String usuario ="";
        try {
            claims = jwt.getClaims(token);
            usuario  = claims.get("sub").toString();
        } catch (Exception e) {
        	
            return "";
        }
        return usuario;
    }
	 

}
