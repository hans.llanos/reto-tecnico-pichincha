package com.pichinga.apirest.security.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.pichinga.apirest.security.entity.Users;

import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveCrudRepository<Users, Long> {

    Mono<Users> findByUsernameOrEmail(String username, String email);
    Mono<Users> findByUsername(String username);
}