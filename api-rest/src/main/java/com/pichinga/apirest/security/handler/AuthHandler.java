package com.pichinga.apirest.security.handler;


import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.pichinga.apirest.security.dto.CreateUserDTO;
import com.pichinga.apirest.security.dto.LoginDTO;
import com.pichinga.apirest.security.dto.TokenDTO;
import com.pichinga.apirest.security.entity.Users;
import com.pichinga.apirest.security.service.UsersService;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@Component

@RequiredArgsConstructor
public class AuthHandler {

    private final UsersService usersService;
    
    public Mono<ServerResponse> login(ServerRequest request) {
        Mono<LoginDTO> dtoMono = request.bodyToMono(LoginDTO.class);
        return dtoMono
                .flatMap(dto -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(usersService.login(dto), TokenDTO.class));
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        Mono<CreateUserDTO> dtoMono = request.bodyToMono(CreateUserDTO.class);
        return dtoMono
                .flatMap(dto -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(usersService.create(dto), Users.class));
    }
}