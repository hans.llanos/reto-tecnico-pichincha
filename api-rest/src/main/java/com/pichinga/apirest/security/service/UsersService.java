package com.pichinga.apirest.security.service;

import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.pichinga.apirest.exception.CustomException;
import com.pichinga.apirest.security.dto.CreateUserDTO;
import com.pichinga.apirest.security.dto.LoginDTO;
import com.pichinga.apirest.security.dto.TokenDTO;
import com.pichinga.apirest.security.entity.Users;
import com.pichinga.apirest.security.enums.Role;
import com.pichinga.apirest.security.jwt.JwtProvider;
import com.pichinga.apirest.security.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UsersService {
	private final UserRepository userRepository;

    private final JwtProvider jwtProvider;

    private final PasswordEncoder passwordEncoder;
    
    

    public Mono<TokenDTO> login(LoginDTO dto) {
        return userRepository.findByUsernameOrEmail(dto.getUsername(), dto.getUsername())
                .filter(user -> passwordEncoder.matches(dto.getPassword(), user.getPassword()))
                .map(user -> new TokenDTO(jwtProvider.generateToken(user)))
                .switchIfEmpty(Mono.error(new CustomException(HttpStatus.BAD_REQUEST, "bad credentials")));
    }

    public Mono<Users> create(CreateUserDTO dto) {
        Users users = Users.builder()
                .username(dto.getUsername())
                .email(dto.getEmail())
                .password(passwordEncoder.encode(dto.getPassword()))
                .roles(Role.ROLE_ADMIN.name() + ", " + Role.ROLE_USER.name())
                .build();
        Mono<Boolean> userExists = userRepository.findByUsernameOrEmail(users.getUsername(), users.getEmail()).hasElement();
        return userExists
                .flatMap(exists -> exists ?
                        Mono.error(new CustomException(HttpStatus.BAD_REQUEST, "username or email already in use"))
                        : userRepository.save(users));
    }
}

