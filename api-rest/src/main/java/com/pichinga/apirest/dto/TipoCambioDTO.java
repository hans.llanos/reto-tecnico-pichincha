package com.pichinga.apirest.dto;

public class TipoCambioDTO {
	
	private String monedaOrigen;
	private String monedaDestino;
	private Double tipoCambio;
	
	private Double monto;
	private Double montoCambiado;
	
	public String getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public String getMonedaDestino() {
		return monedaDestino;
	}
	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}	
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Double getMontoCambiado() {
		return montoCambiado;
	}
	public void setMontoCambiado(Double montoCambiado) {
		this.montoCambiado = montoCambiado;
	}
	@Override
	public String toString() {
		return "TipoCambioDTO [monedaOrigen=" + monedaOrigen + ", monedaDestino=" + monedaDestino + ", tipoCambio="
				+ tipoCambio + ", monto=" + monto + ", montoCambiado=" + montoCambiado + "]";
	}
	
}
