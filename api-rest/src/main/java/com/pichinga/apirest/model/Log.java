package com.pichinga.apirest.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import nonapi.io.github.classgraph.json.Id;

@Getter
@Setter
public class Log {
	@Id
	private Long id;
	private String accion;
	private String detalle;
	private String usuario;
	private Date fechaCreacion;
	

}
