package com.pichinga.apirest.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;


import com.pichinga.apirest.model.Usuario;

import reactor.core.publisher.Flux;

@Repository
public interface UsuarioRepository extends R2dbcRepository<Usuario, Long>  {

	Flux<Usuario> findOneByEmail(String email);
}
