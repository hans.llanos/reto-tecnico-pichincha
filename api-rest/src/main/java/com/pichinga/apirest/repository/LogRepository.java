package com.pichinga.apirest.repository;


import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.pichinga.apirest.model.Log;

@Repository
public interface LogRepository extends R2dbcRepository<Log, Long>  {

}
