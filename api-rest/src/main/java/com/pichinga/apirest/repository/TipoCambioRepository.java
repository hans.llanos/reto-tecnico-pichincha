package com.pichinga.apirest.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import com.pichinga.apirest.model.TipoCambio;

import reactor.core.publisher.Mono;

@Repository
public interface TipoCambioRepository extends R2dbcRepository<TipoCambio, Long>  {
	
	Mono<TipoCambio> findByMonedaOrigenAndMonedaDestino(String monedaOrigen, String monedaDestino);
	
}
